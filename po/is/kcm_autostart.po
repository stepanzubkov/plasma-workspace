# translation of kcm_autostart.po to Icelandic
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2008, 2010, 2016, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:39+0000\n"
"PO-Revision-Date: 2023-09-09 09:51+0000\n"
"Last-Translator: Guðmundur Erlingsson <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.04.3\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"
"\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" er ekki algild slóð."

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1\" er ekki til."

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" er ekki skrá."

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" er ekki læsileg."

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "Heiti:"

#: ui/entry.qml:58
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "Staða:"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr "Síðast virkjað:"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr "Stöðva"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "Ræsa"

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr "Ekki tókst að hlaða annála. Prófaðu að endurhlaða."

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr "Endurhlaða"

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "Gera keyranlegt"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr ""
"Skráin '%1' verður að vera keyranleg (executable) til að hægt sé að ræsa "
"hana við útskráningu."

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr ""
"Skráin '%1' verður að vera keyranleg (executable) til að hægt sé að ræsa "
"hana við innskráningu."

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr "Bæta við…"

#: ui/main.qml:69
#, fuzzy, kde-format
#| msgid "Add Application…"
msgctxt "@action:button"
msgid "Add Application…"
msgstr "Bæta við forriti…"

#: ui/main.qml:74
#, fuzzy, kde-format
#| msgid "Add Login Script…"
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "Bæta við innskráningarskriftu…"

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Add Logout Script…"
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "Bæta við útskráningarskriftu…"

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""
"%1 hefur ekki enn verið ræst sjálfvirkt. Upplýsingar verða tiltækar þegar "
"tölvan hefur verið endurræst."

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr "Ekki enn ræst sjálfvirkt"

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr "Sjá eiginleika"

#: ui/main.qml:157
#, kde-format
msgctxt "@action:button"
msgid "Remove entry"
msgstr "Fjarlægja færslu"

#: ui/main.qml:173
#, kde-format
msgid "Applications"
msgstr "Forrit"

#: ui/main.qml:176
#, kde-format
msgid "Login Scripts"
msgstr "Innskráningarskriftur"

#: ui/main.qml:179
#, kde-format
msgid "Pre-startup Scripts"
msgstr "Forræsingarskriftur"

#: ui/main.qml:182
#, kde-format
msgid "Logout Scripts"
msgstr "Útskráningarskriftur"

#: ui/main.qml:191
#, kde-format
msgid "No user-specified autostart items"
msgstr "Engin sjálfræsandi atriði skilgreind af notanda"

#: ui/main.qml:192
#, fuzzy, kde-kuit-format
#| msgctxt "@info"
#| msgid "Click the <interface>Add…</interface> button below to add some"
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr ""
"Smelltu á <interface>Bæta við…</interface> hér fyrir neðan til að bæta við"

#: ui/main.qml:207
#, kde-format
msgid "Choose Login Script"
msgstr "Veldu innskráningarskriftu"

#: ui/main.qml:227
#, kde-format
msgid "Choose Logout Script"
msgstr "Veldu útskráningarskriftu"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr "Í keyrslu"

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr "Ekki í keyrslu"

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "Ræsi"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr "Stöðva"

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr "Mistókst"

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr "Villa kom upp þegar svar barst við GetAll-kalli %1"

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr "Mistókst að opna færsluskrá"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Richard Allen, Sveinn í Felli"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "ra@ra.is, sv1@fellsnet.is"

#, fuzzy
#~| msgid "KDE Autostart Manager Control Panel Module"
#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "Stjórnspjaldseining KDE sjálfræsistjórans"

#, fuzzy
#~| msgid "Copyright © 2006–2010 Autostart Manager team"
#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Höfundarréttur © 2006–2010 KDE sjálfræsistjórateymið"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "Umsjónarmaður"

#, fuzzy
#~| msgid "Advanced"
#~ msgid "Add..."
#~ msgstr "Nánar"

#, fuzzy
#~| msgid "Shell script:"
#~ msgid "Shell script path:"
#~ msgstr "Skeljarskrifta:"

#~ msgid "Create as symlink"
#~ msgstr "Búa til sem symlink"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "Sjálfræsa eingöngu með KDE"

#~ msgid "Command"
#~ msgstr "Skipun"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "Keyra á"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "KDE sjálfræsistjóri"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "Virkt"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "Óvirkt"

#~ msgid "Desktop File"
#~ msgstr "Skjáborðsskrá"

#~ msgid "Script File"
#~ msgstr "Skriftuskrá"

#~ msgid "Add Program..."
#~ msgstr "Bæta við forriti"

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr ""
#~ "KDE les eingöngu skrár með “.sh” endingum við uppsetningu umhverfisins."

#~ msgid "Shutdown"
#~ msgstr "Slökkva"
